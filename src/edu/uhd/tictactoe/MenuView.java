package edu.uhd.tictactoe;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class MenuView extends VBox{
	
	public MenuView() {
		Label title = new Label("TicTacToe");
		title.setFont(Font.font(90));
		title.setMaxWidth(Double.MAX_VALUE);
		title.setMaxHeight(Double.MAX_VALUE);
		title.setMinHeight(150);
		title.setAlignment(Pos.CENTER);
		VBox.setVgrow(title, Priority.ALWAYS);
		this.getChildren().add(title);
		
		Button single = new Button("Single Player");
		single.setMaxWidth(Double.MAX_VALUE);
		single.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(single, Priority.ALWAYS);
		this.getChildren().add(single);
		single.setOnAction((e) -> GameWindow.border.setCenter(new DifficultySelectorView()));
		
		Button multi = new Button("Multiplayer");
		multi.setMaxWidth(Double.MAX_VALUE);
		multi.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(multi, Priority.ALWAYS);
		this.getChildren().add(multi);
		multi.setOnAction((e) -> GameWindow.border.setCenter(new PlayerInputView()));
		
		Button history = new Button("View History");
		history.setMaxWidth(Double.MAX_VALUE);
		history.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(history, Priority.ALWAYS);
		this.getChildren().add(history);
		history.setOnAction(e -> GameWindow.border.setCenter(new HistoryView()));
	}
	
}
