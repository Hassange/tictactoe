package edu.uhd.tictactoe;

import java.util.Arrays;

public class Board {
	
	public final int BOARD_LENGTH = 5;
	public final int TOWIN = 4;
//	public final int DIAG_COUNT = 6;
	
//	int[] scores = new int[BOARD_LENGTH * 2 + DIAG_COUNT];
	
	Tile[][] tiles = new Tile[5][5];
	Player player1, player2;
	Player turn; // The player whose turn it is, true for player1
	Player winner = null;
	
	int boardStrength = 0;
	
	public Board(Board old) {
//		this.tiles = old.tiles;
		this.player1 = old.player1;
		this.player2 = old.player2;
		this.turn = old.turn;
		this.winner = old.winner;
		for(int x = 0; x < BOARD_LENGTH; x++)
			for(int y = 0; y < BOARD_LENGTH; y++)
				tiles[x][y] = old.tiles[x][y];
	}
	
	public Board(Player player1, Player player2) {
		this.player1 = player1;
		this.player2 = player2;
		// Make all the tiles blank:
		for(Tile[] row : tiles)
			Arrays.fill(row, Tile.BLANK);
		turn = player1;
	}
	
	public void play(int x, int y) {
		if(x < 0 || x > 4 || y < 0 || y > 4)
			return;
		tiles[x][y] = turn.marker;
		
		turn = turn == player1? player2: player1;
		winner = calculateWinner();
	}
	
	public Player getWinner() {
		return winner;
	}
	
	public int getBoardStrength() {
		return boardStrength;
	}
	
	private Player calculateWinner() {
		// Only check the condition for the most recent player
		Tile tile = getOtherPlayer().marker;
		
		boardStrength = 0;
		
		// Check vertical win
		for(int x = 0; x < BOARD_LENGTH; x++)
			for(int y = 0; y <= BOARD_LENGTH - TOWIN; y++) {
				int count = 0;
				while(tiles[x][y] == tile) {
					y++; count++;
					if(count == TOWIN)
						return getOtherPlayer();
				}
				boardStrength += count;
				count = 0;
			}
		
		// Check horizontal win
		for(int y = 0; y < BOARD_LENGTH; y++)
			for(int x = 0; x <= BOARD_LENGTH - TOWIN; x++) {
				int count = 0;
				while(tiles[x][y] == tile) {
					x++; count++;
					if(count == TOWIN)
						return getOtherPlayer();
				}
				boardStrength += count;
				count = 0;
			}
		
		// Check diagonal win
		for(int y = 0; y <= BOARD_LENGTH - TOWIN; y++)
			for(int x = 0; x <= BOARD_LENGTH - TOWIN; x++) {
				int count = 0;
				while(tiles[x][y] == tile) {
					x++; y++; count++;
					if(count == TOWIN)
						return getOtherPlayer();
				}
				boardStrength += count;
				count = 0;
			}
		
		// Check for anti-diagonal win
		for(int y = BOARD_LENGTH - 1; y >= TOWIN - 1; y--)
			for(int x = 0; x <= BOARD_LENGTH - TOWIN; x++) {
				int count = 0;
				while(tiles[y][x] == tile) {
					x++; y--; count++;
					if(count == TOWIN)
						return getOtherPlayer();
				}
				boardStrength += count;
				count = 0;
			}
		
		return null;
	}
	
	public Tile getTile(int x, int y) {
		if(x < 0 || x > 4 || y < 0 || y > 4)
			return null;
		return tiles[x][y];
	}
	
	public Player getPlayer1() {
		return player1;
	}
	
	public Player getPlayer2() {
		return player2;
	}
	
	public Player getPlayerWhoseTurnItIs() {
		return turn;
	}
	
	// Get player whose turn it isn't
	public Player getOtherPlayer() {
		if(turn == player1)
			return player2;
		else
			return player1;
	}
	
	public String toString() {
		String s = "";
		for(Tile[] cols : tiles) {
			for(Tile tile : cols) {
				switch(tile) {
				case X: s += "X"; break;
				case O: s += "O"; break;
				case BLANK: s += "" + (char)0x00B7;
				}
			}
			s += "\n";
		}
		return s;
	}
	
}
