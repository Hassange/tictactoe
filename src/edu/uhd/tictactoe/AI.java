package edu.uhd.tictactoe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AI extends Player {
	
	int n = 0;
	int LEVEL = 0;
	
	public AI(String name, Tile marker, int level) {
		super(name, marker);
		this.LEVEL = level;
	}
	
	@Override
	public boolean isAI() {
		return true;
	}
	
	// A structure to store a move
	class Move {
		public int x, y, score = 0;
		public Move(int x, int y) {
			this.x = x;
			this.y = y;
		}
		public String toString() { return "(" + x + ", " + y + ")"; }
	}
	
	public int evaluateBoard(Board board) {
//		int r = board.getBoardStrength();
		int r = 0;
		
		Tile tile = board.getOtherPlayer().marker;
		
		// Vertical
		for(int x = 0; x < board.BOARD_LENGTH; x++)
			for(int y = 0; y <= board.BOARD_LENGTH - board.TOWIN; y++) {
				int count = 0;
//				while(board.getTile(x, y) == tile) {
//					y++; count++;
//				}
				Tile encountered = Tile.BLANK;
				
				for(int i = 0; i < board.TOWIN; i++) {
					Tile current = board.getTile(x, y + i);
					
					if(current != Tile.BLANK) {
						if(encountered == Tile.BLANK)
							encountered = current;
						else if(encountered != current) {
							count = 0; break;
						}
					}
					
					if(current == tile) count++;
					else if(current != null && current != Tile.BLANK) count--;
				}
//				r -= count;
				if(count > 3) r += 100;
				else if(count < -3) r -= 100;
				else if(count > 2) r += 30;
				else if(count < -2) r -= 30;
				else if(count < -1) r -= 3;
				else r += count;
				count = 0;
			}

		// Horizontal
		for(int y = 0; y < board.BOARD_LENGTH; y++)
			for(int x = 0; x <= board.BOARD_LENGTH - board.TOWIN; x++) {
//				int count = 0;
//				while(board.getTile(x, y) == tile) {
//					x++; count++;
//				}
//				r -= count;
//				count = 0;
				int count = 0;
				Tile encountered = Tile.BLANK;
				for(int i = 0; i < board.TOWIN; i++) {
					Tile current = board.getTile(x + i, y);
					
					if(current != Tile.BLANK) {
						if(encountered == Tile.BLANK)
							encountered = current;
						else if(encountered != current) {
							count = 0; break;
						}
					}
					
					if(current == tile) count++;
					else if(current != null && current != Tile.BLANK) count--;
				}
				if(count > 3) r += 100;
				else if(count < -3) r -= 100;
				else if(count > 2) r += 30;
				else if(count < -2) r -= 30;
				else if(count < -1) r -= 3;
				else r += count;
				count = 0;
			}

		// Diagonal
		for(int y = 0; y <= board.BOARD_LENGTH - board.TOWIN; y++)
			for(int x = 0; x <= board.BOARD_LENGTH - board.TOWIN; x++) {
//				int count = 0;
//				while(board.getTile(x, y) == tile) {
//					x++; y++; count++;
//				}
//				r -= count;
//				count = 0;
				
				int count = 0;
				Tile encountered = Tile.BLANK;
				
				for(int i = 0; i < board.TOWIN; i++) {
					Tile current = board.getTile(x + i, y + i);
					
					if(current != Tile.BLANK) {
						if(encountered == Tile.BLANK)
							encountered = current;
						else if(encountered != current) {
							count = 0; break;
						}
					}
					
					if(current == tile) count++;
					else if(current != null && current != Tile.BLANK) count--;
				}
				if(count > 3) r += 100;
				else if(count < -3) r -= 100;
				else if(count > 2) r += 30;
				else if(count < -2) r -= 30;
				else if(count < -1) r -= 3;
				else r += count;
				count = 0;
			}

		// Anti-diagonal
		for(int y = board.BOARD_LENGTH - 1; y >= board.TOWIN - 1; y--)
			for(int x = 0; x <= board.BOARD_LENGTH - board.TOWIN; x++) {
//				int count = 0;
//				while(board.getTile(x, y) == tile) {
//					x++; y--; count++;
//				}
//				r -= count;
//				count = 0;
				int count = 0;
				Tile encountered = Tile.BLANK;
				
				for(int i = 0; i < board.TOWIN; i++) {
					Tile current = board.getTile(x + i, y - i);
					
					if(current != Tile.BLANK) {
						if(encountered == Tile.BLANK)
							encountered = current;
						else if(encountered != current) {
							count = 0; break;
						}
					}
					
					if(current == tile) count++;
					else if(current != null && current != Tile.BLANK) count--;
				}
				
				if(count > 3) r += 100;
				else if(count < -3) r -= 100;
				else if(count > 2) r += 30;
				else if(count < -2) r -= 30;
//				else if(count > 1) r += 5;
				else if(count < -1) r -= 3;
				else r += count;
				count = 0;
			}
		
//		if(board.getWinner() == this)
//			r += 100;
//		else if(board.getWinner() != null)
//			r -= 100;
		
		return r;
	}
	
	List<Move> moves(Board board) {
		List<Move> possibleMoves = new ArrayList<Move>();
		for(int x = 0; x < board.BOARD_LENGTH; x++)
			for(int y = 0; y < board.BOARD_LENGTH; y++)
				if(board.getTile(x, y) == Tile.BLANK)
					possibleMoves.add(new Move(x, y));
		
		if(possibleMoves.isEmpty()) return possibleMoves;
		
		if(n < LEVEL) {
			for(Move move : possibleMoves) {
				Board virtual = new Board(board);
				virtual.play(move.x, move.y);
				if(virtual.getWinner() != null) {
					move.score = evaluateBoard(virtual);
					continue;
				}
				List<Move> moves = moves(virtual);
				if(!moves.isEmpty())
					move.score = -Collections.min(moves, Comparator.comparing(c -> c.score)).score;
			}
		} else {
			for(Move move : possibleMoves) {
				Board virtual = new Board(board);
				virtual.play(move.x, move.y);
				move.score = evaluateBoard(virtual);
			}
		}
		
		n++;
		
		return possibleMoves;
	}
	
	public int play(Board board) {
		n = 0;
		List<Move> moves = moves(board);
		
		if(moves.isEmpty()) return 0;
		Move bestMove = Collections.max(moves, Comparator.comparing(c -> c.score));
		board.play(bestMove.x, bestMove.y);
		
		GameWindow.stage.setTitle("");
		
//		board.play(possibleMoves.get(0).x, possibleMoves.get(0).y);
		
		return 0;
	}
	
}
