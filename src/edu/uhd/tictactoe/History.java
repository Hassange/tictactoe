package edu.uhd.tictactoe;

import java.util.ArrayList;
import java.util.List;

public class History {
	
	private List<Board> games = new ArrayList<Board>();
	
	public void addGame(Board game) {
		games.add(game);
		GameWindow.save();
	}
	
	public List<Board> getGames() {
		return this.games;
	}
	
}
