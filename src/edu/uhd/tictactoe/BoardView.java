package edu.uhd.tictactoe;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;

public class BoardView extends BorderPane {
	
	HBox bottomBar = new HBox();
	GridPane boardPane = new GridPane();
	
	Board board;
	
	String x = "" + (char)0x2A09;
	String o = "" + (char)0x25EF;
	String blank = "" + (char)0x2022;
	
	// References to all the buttons on the grid
	Button[][] buttons;
	
	Button again;
	
	private void refresh() {
		HBox playerPane = new HBox();
//		playerPane.setPadding(new Insets(20, 20, 20, 20));
		playerPane.setSpacing(0);
		playerPane.setStyle("-fx-background-color: #4CBB17");
		Label turnLabel = new Label(board.getPlayerWhoseTurnItIs().name + "'s turn");
		HBox.setHgrow(turnLabel, Priority.ALWAYS);
		turnLabel.setFont(Font.font(50));
		turnLabel.setAlignment(Pos.CENTER);
//		player1Label.setBackground(Background);
		turnLabel.setStyle("-fx-background-color: #00000000;");
		playerPane.getChildren().addAll(turnLabel);
		
		if(board.getWinner() != null) {
			turnLabel.setText(board.getWinner().getName() + " wins!");
			again.setVisible(true);
			GameWindow.history.addGame(board);
		}
		
		this.setTop(playerPane);
		
		boolean draw = true;
		
		for(int col = 0; col < board.BOARD_LENGTH; col++)
			for(int row = 0; row < board.BOARD_LENGTH; row++) {
				Tile tile = board.getTile(col, row);
				if(tile == Tile.BLANK) draw = false;
				if(tile == Tile.BLANK)
					buttons[col][row].setText(blank);
				else if(tile == Tile.X)
					buttons[col][row].setText(x);
				else if(tile == Tile.O)
					buttons[col][row].setText(o);
			}
		
		if(draw) {
			turnLabel.setText("It's a draw!");
			again.setVisible(true);
			GameWindow.history.addGame(board);
		}
	}
	
	public BoardView(Board board) {
		this.board = board;
		
		this.getStyleClass().add("pane");
		this.setCenter(boardPane);
		this.setBottom(bottomBar);
		
		buttons = new Button[board.BOARD_LENGTH][board.BOARD_LENGTH];
		
		for(int i = 0; i < board.BOARD_LENGTH * board.BOARD_LENGTH; i++) {
			int column = i % board.BOARD_LENGTH;
			int row = (int)(i / board.BOARD_LENGTH);
			Button button = new Button(blank);
			button.setStyle("-fx-font: 50 arial; -fx-base: #b6e7c9;");
			boardPane.getChildren().add(button);
			GridPane.setRowIndex(button, row);
			GridPane.setColumnIndex(button, column);
			GridPane.setHgrow(button, Priority.ALWAYS);
			GridPane.setVgrow(button, Priority.ALWAYS);
			GridPane.setHalignment(button, HPos.CENTER);
			buttons[column][row] = button;
			button.setOnAction(e -> {
				if(!button.getText().equals(blank) || board.getWinner() != null)
					return;
//				button.setText(board.getPlayerWhoseTurnItIs().marker.toString());
				board.play(column, row);
				if(board.getWinner() == null && board.getPlayerWhoseTurnItIs().isAI())
					((AI)board.getPlayerWhoseTurnItIs()).play(board);
				refresh();
			});
		}
		
		// If the AI is going first, make it go
		if(board.getPlayerWhoseTurnItIs().isAI())
			((AI)board.getPlayerWhoseTurnItIs()).play(board);
		
		Button exit = new Button("Exit");
		exit.setOnAction(e -> GameWindow.border.setCenter(new MenuView()));
		
		again = new Button("Again!");
		again.setOnAction(e -> GameWindow.border.setCenter(new BoardView(new Board(this.board.player1, this.board.player2))) );
		again.setVisible(false);
		
		bottomBar.getChildren().addAll(exit, again);
		
		refresh();
		
	}
	
}
