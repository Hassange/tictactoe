package edu.uhd.tictactoe;

public class Player {
	
	String name;
	Tile marker;
	
	public Player(String name, Tile marker) {
		if(marker == Tile.BLANK)
			return;
		this.name = name;
		this.marker = marker;
	}
	
	public String toString() {
		return this.name;
	}
	
	public boolean isAI() {
		return false;
	}
	
	public String getName() {
		return name;
	}
	public Tile getMarker() {
		return marker;
	}
	
}
