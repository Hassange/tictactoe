package edu.uhd.tictactoe;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class SinglePlayerInputView extends VBox {
	
	TextField name;
	
	private void startGame(boolean first, int level) {
		String humanName = "Human";
		if(!name.getText().trim().isEmpty())
			humanName = name.getText().trim();
		
		if(first) {
			Player ai = new AI("Computer", Tile.O, level);
			Player human = new Player(humanName, Tile.X);
			Board board = new Board(human, ai);
			GameWindow.border.setCenter(new BoardView(board));
		} else {
			Player ai = new AI("Computer", Tile.X, level);
			Player human = new Player(humanName, Tile.O);
			Board board = new Board(ai, human);
			GameWindow.border.setCenter(new BoardView(board));
		}
	}
	
	public SinglePlayerInputView(int level) {
		Label title = new Label("Single Player");
		title.setFont(Font.font(90));
		title.setMaxWidth(Double.MAX_VALUE);
		title.setMaxHeight(Double.MAX_VALUE);
		title.setAlignment(Pos.CENTER);
		VBox.setVgrow(title, Priority.ALWAYS);
		this.getChildren().add(title);
		
		HBox player1Pane = new HBox();
		player1Pane.setPadding(new Insets(10, 10, 10, 10));
		player1Pane.setSpacing(30);
		player1Pane.setStyle("-fx-background-color: #4CBB17");
		name = new TextField();
		name.setPromptText("<blank for anonymous>");
		HBox.setHgrow(name, Priority.ALWAYS);
		Label player1Label = new Label("Name: ");
		player1Label.setFont(Font.font(20));
		player1Label.setStyle("-fx-background-color: #00000000;");
		player1Pane.getChildren().addAll(player1Label, name);
		this.getChildren().add(player1Pane);
		
		ToggleButton first = new ToggleButton("Play as X");
		first.setMaxWidth(Double.MAX_VALUE);
		first.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(first, Priority.ALWAYS);
		this.getChildren().add(first);
		first.setOnAction(e -> startGame(true, level));
		
		ToggleButton second = new ToggleButton("Play as O");
		second.setMaxWidth(Double.MAX_VALUE);
		second.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(second, Priority.ALWAYS);
		this.getChildren().add(second);
		second.setOnAction(e -> startGame(false, level));
		
		
	}
	
}
