package edu.uhd.tictactoe;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class DifficultySelectorView extends VBox {
	
	private void proceed(int level) {
		GameWindow.border.setCenter(new SinglePlayerInputView(level));
	}
	
	public DifficultySelectorView() {
		Label title = new Label("Select difficulty");
		title.setFont(Font.font(90));
		title.setMaxWidth(Double.MAX_VALUE);
		title.setMaxHeight(Double.MAX_VALUE);
		title.setAlignment(Pos.CENTER);
		VBox.setVgrow(title, Priority.ALWAYS);
		this.getChildren().add(title);
		
//		ToggleGroup difficultyGroup = new ToggleGroup();
		
		Button easy = new Button("Easy");
		easy.setMaxWidth(Double.MAX_VALUE);
		easy.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(easy, Priority.ALWAYS);
		this.getChildren().add(easy);
		easy.setOnAction(e -> proceed(1));
//		easy.setToggleGroup(difficultyGroup);
		
		Button medium = new Button("Medium");
		medium.setMaxWidth(Double.MAX_VALUE);
		medium.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(medium, Priority.ALWAYS);
		this.getChildren().add(medium);
		medium.setOnAction(e -> proceed(2));
//		medium.setToggleGroup(difficultyGroup);
		
		Button hard = new Button("Hard");
		hard.setMaxWidth(Double.MAX_VALUE);
		hard.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(hard, Priority.ALWAYS);
		this.getChildren().add(hard);
		hard.setOnAction(e -> proceed(10));
//		hard.setToggleGroup(difficultyGroup);
		
		Button back = new Button("Back");
		back.setMaxWidth(Double.MAX_VALUE);
		back.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(back, Priority.ALWAYS);
		this.getChildren().add(back);
		back.setOnAction((e) -> GameWindow.border.setCenter(new MenuView()));
	}
	
}
