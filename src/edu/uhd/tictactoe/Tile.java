package edu.uhd.tictactoe;

public enum Tile {
	BLANK, X, O
}
