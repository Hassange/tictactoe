package edu.uhd.tictactoe;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.thoughtworks.xstream.XStream;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class GameWindow extends Application {
	
	public static BorderPane border = new BorderPane();
	public static Stage stage;
	public static History history;
	
	private static final String saveFile = "history.xml";
	
	public static void save() {
		XStream xstream = new XStream();
		File file = new File(saveFile);
		System.out.println(file.getAbsolutePath());
		try {
			xstream.toXML(history, new FileWriter(file));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void load() {
		File file = new File(saveFile);
		if(!file.exists())
			history = new History();
		else {
			XStream xstream = new XStream();
			history = (History) xstream.fromXML(file);
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage stage) throws Exception {
		GameWindow.stage = stage;
		
		stage.setTitle("TicTacToe");
		
		Scene scene = new Scene(border, 400, 500);
		stage.setScene(scene);
		
		scene.getStylesheets().clear();
		scene.getStylesheets().add("resources/dark.css");
		
		border.setCenter(new MenuView());
		
		load();
		
		stage.show();
	}
	
}
