package edu.uhd.tictactoe;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class HistoryView extends VBox {
	
	public HistoryView() {
		VBox historyList = new VBox();
		ScrollPane scroll = new ScrollPane();
		scroll.setContent(historyList);
		scroll.setMaxWidth(Double.MAX_VALUE);
//		scroll.setMaxHeight(Double.MAX_VALUE);
		scroll.setVmax(Double.MAX_VALUE);
//		VBox.setVgrow(scroll, Priority.ALWAYS);
		
		for(int i = 0; i < 30; i++) {
			if(GameWindow.history.getGames().size() < i+1) break;
			
			Board game = GameWindow.history.getGames().get(i);
			Player winner = game.getWinner();
			String titleString = game.player1 + " vs " + game.player2;
			String outcomeString = "Draw";
			if(winner != null) outcomeString = winner + " wins.";
			Label label = new Label(titleString + "(" + outcomeString + ")\n");
			label.setWrapText(true);
			label.setFont(new Font(30));
			label.setMaxWidth(Double.MAX_VALUE);
			
			historyList.getChildren().add(label);
		}
		
		Button back = new Button("Back");
		back.setFont(new Font(90));
		back.setOnAction(e -> GameWindow.border.setCenter(new MenuView()));
		back.setMaxWidth(Double.MAX_VALUE);
		back.setMaxHeight(Double.MAX_VALUE);
		this.getChildren().addAll(historyList, back);
	}
	
}
