package edu.uhd.tictactoe;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class PlayerInputView extends VBox {
	
	public PlayerInputView() {
		Label title = new Label("Who's Playing?");
		title.setFont(Font.font(90));
		title.setMaxWidth(Double.MAX_VALUE);
		title.setMaxHeight(Double.MAX_VALUE);
		title.setMinHeight(150);
		title.setAlignment(Pos.CENTER);
		VBox.setVgrow(title, Priority.ALWAYS);
		this.getChildren().add(title);
		
		// TODO put in input forms
		
		HBox player1Pane = new HBox();
		player1Pane.setPadding(new Insets(10, 10, 10, 10));
		player1Pane.setSpacing(30);
		player1Pane.setStyle("-fx-background-color: #4CBB17");
		TextField player1Name = new TextField();
		HBox.setHgrow(player1Name, Priority.ALWAYS);
		Label player1Label = new Label("First: ");
		player1Label.setFont(Font.font(20));
//		player1Label.setBackground(Background);
		player1Label.setStyle("-fx-background-color: #00000000;");
		player1Pane.getChildren().addAll(player1Label, player1Name);
		this.getChildren().add(player1Pane);
		
		HBox player2Pane = new HBox();
		player2Pane.setPadding(new Insets(10, 10, 10, 10));
		player2Pane.setSpacing(30);
		player2Pane.setStyle("-fx-background-color: #FE2712");
		TextField player2Name = new TextField();
		HBox.setHgrow(player2Name, Priority.ALWAYS);
		Label player2Label = new Label("Second:");
		player2Label.setFont(Font.font(20));
//		player1Label.setBackground(Background);
		player2Label.setStyle("-fx-background-color: #00000000;");
		player2Pane.getChildren().addAll(player2Label, player2Name);
		this.getChildren().add(player2Pane);
		
		Button start = new Button("Start");
		start.setMaxWidth(Double.MAX_VALUE);
		start.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(start, Priority.ALWAYS);
		this.getChildren().add(start);
		start.setOnAction(e -> {
			if(player1Name.getText().trim().equals("") || player1Name.getText().trim().equals(""))
				return;
			Player player1 = new Player(player1Name.getText(), Tile.X);
			Player player2 = new Player(player2Name.getText(), Tile.O);
			GameWindow.border.setCenter(new BoardView(new Board(player1, player2)));
		});
		
		Button back = new Button("Back");
		back.setMaxWidth(Double.MAX_VALUE);
		back.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(back, Priority.ALWAYS);
		this.getChildren().add(back);
		back.setOnAction(e -> GameWindow.border.setCenter(new MenuView()));
	}
	
}
